FROM openjdk:8u181-nanoserver
ARG JAR_FILE=/project-syn-0.1.0.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Dspring.config.location=file:///${CONFIG_FILE}","-jar","/app.jar"]
