package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "im3_tema")
public class Tema implements Serializable {

    private static final long serialVersionUID = 158211316044920116L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTema;

    @Column
    private String nombre;

    @Column(name = "activo")
    private boolean isActivo;

    @Column(name = "sincronizado")
    private boolean isSincronizado;

    @Column
    private Date fechaCreacion;

    @Column
    private Date fechaActualizacion;

    @JsonIgnore
    @OneToOne(mappedBy = "tema")
    private Institucion institucion;

    public int getIdTema() {
        return idTema;
    }

    public void setIdTema(int idTema) {
        this.idTema = idTema;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isActivo() {
        return isActivo;
    }

    public void setActivo(boolean isActivo) {
        this.isActivo = isActivo;
    }

    @JsonIgnore
    public boolean isSincronizado() {
        return isSincronizado;
    }

    public void setSincronizado(boolean isSincronizado) {
        this.isSincronizado = isSincronizado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @JsonIgnore
    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }

}
