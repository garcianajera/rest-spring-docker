package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Phrase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PhraseRepository extends CrudRepository<Phrase, Integer> {

    List<Phrase> findByIdTema(int idTema);

    List<Phrase> findByIsSincronizadoIsFalse();

}
