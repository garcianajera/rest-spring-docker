package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Medio;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedioRepository extends CrudRepository<Medio, Integer> {

    List<Medio> findByIsSincronizadoIsFalse();

}
