package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "im3_noticia_tema")
public class NoticiaTema implements Serializable {

    private static final long serialVersionUID = -4356668603051637604L;
    @EmbeddedId
    private NoticiaTemaPk noticiaTemaPk;

    @Column
    private float peso;

    @Column
    private String detalle;

    @Column
    private boolean estatus;

    @JsonIgnore
    @Column(name = "sincronizado")
    private boolean isSincronizado;

    @Column
    private boolean estatusUsuario;

    @Column
    private Date fechaCreacion;

    @JsonIgnore
    @Column
    private Date fechaActualizacion;

    public NoticiaTemaPk getNoticiaTemaPk() {
        return noticiaTemaPk;
    }

    public void setNoticiaTemaPk(NoticiaTemaPk noticiaTemaPk) {
        this.noticiaTemaPk = noticiaTemaPk;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    @JsonIgnore
    public boolean isSincronizado() {
        return isSincronizado;
    }

    public void setSincronizado(boolean isSincronizado) {
        this.isSincronizado = isSincronizado;
    }

    public boolean getEstatusUsuario() {
        return estatusUsuario;
    }

    public void setEstatusUsuario(boolean estatusUsuario) {
        this.estatusUsuario = estatusUsuario;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

}
