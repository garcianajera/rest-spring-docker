package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.Tipo;

import java.util.List;



public interface TipoService {

    Tipo crear(Tipo entity);

    Tipo actualizar(Tipo entity);

    List<Tipo> list();

    Tipo getById(int id);

}
