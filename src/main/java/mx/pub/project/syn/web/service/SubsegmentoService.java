package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.Subsegmento;

import java.util.List;



public interface SubsegmentoService {
    Subsegmento crear(Subsegmento entity);

    Subsegmento actualizar(Subsegmento entity);

    List<Subsegmento> list();

    Subsegmento getById(int id);

    List<Subsegmento> list(Integer idTipo);

}
