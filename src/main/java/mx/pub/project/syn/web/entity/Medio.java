package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "im3_medio")
public class Medio implements Serializable {

    private static final long serialVersionUID = -3940152451796131867L;

    @javax.persistence.Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idMedio;

    @Column
    private String tipo;

    @Column(name = "prioritario")
    private boolean isPrioritario;

    @Column
    private String nombre;

    @Column
    private String url;

    @Column
    private String perfilTwitter;

    @Column
    private Date fechaCreacion;

    @Column(name = "activo")
    private boolean isActivo;

    @JsonIgnore
    @Column
    private Date fechaActualizacion;

    @JsonIgnore
    @Column(name = "sincronizado")
    private boolean isSincronizado;

    public int getIdMedio() {
        return idMedio;
    }

    public void setIdMedio(int idMedio) {
        this.idMedio = idMedio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isPrioritario() {
        return isPrioritario;
    }

    public void setPrioritario(boolean isPrioritario) {
        this.isPrioritario = isPrioritario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPerfilTwitter() {
        return perfilTwitter;
    }

    public void setPerfilTwitter(String perfilTwitter) {
        this.perfilTwitter = perfilTwitter;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public boolean isSincronizado() {
        return isSincronizado;
    }

    public void setSincronizado(boolean isSincronizado) {
        this.isSincronizado = isSincronizado;
    }

    public boolean isActivo() {
        return isActivo;
    }

    public void setActivo(boolean isActivo) {
        this.isActivo = isActivo;
    }

}
