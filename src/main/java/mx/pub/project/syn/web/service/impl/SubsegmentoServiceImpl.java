package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Subsegmento;
import mx.pub.project.syn.web.repository.SubsegmentoRepository;
import mx.pub.project.syn.web.service.SubsegmentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SubsegmentoServiceImpl implements SubsegmentoService {

    @Autowired
    private SubsegmentoRepository subsegmentoRepository;

    @Override
    public Subsegmento crear(Subsegmento entity) {
        return actualizar(entity);
    }

    @Override
    public Subsegmento actualizar(Subsegmento entity) {
        entity.setFechaActualizacion(new Date());
        return subsegmentoRepository.save(entity);
    }

    @Override
    public List<Subsegmento> list() {
        List<Subsegmento> list = new ArrayList<>();
        Iterable<Subsegmento> iter = subsegmentoRepository.findAll();
        iter.forEach(list::add);
        return list;
    }

    @Override
    public Subsegmento getById(int id) {
        return subsegmentoRepository.findOne(id);
    }

    @Override
    public List<Subsegmento> list(Integer idTipo) {
        return subsegmentoRepository.findByTipoIdTipo(idTipo);
    }

}
