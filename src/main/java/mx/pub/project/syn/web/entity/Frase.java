package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "im3_Phrase")
public class Phrase implements Serializable {

    private static final long serialVersionUID = 6318717492025766102L;

    @javax.persistence.Id
    @Column(name = "id_Phrase")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPhrase;

    @Column
    private float peso;

    @Column
    private String Phrase;

    @Column(name = "activo")
    private boolean isActivo;

    @Column(name = "sincronizado")
    private boolean isSincronizado;

    @Column
    private Date fechaCreacion;

    @Column
    private Date fechaActualizacion;

    @Column
    private int idTema;

    public int getIdPhrase() {
        return idPhrase;
    }

    public void setIdPhrase(int idPhrase) {
        this.idPhrase = idPhrase;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getPhrase() {
        return Phrase;
    }

    public void setPhrase(String Phrase) {
        this.Phrase = Phrase;
    }

    public boolean isActivo() {
        return isActivo;
    }

    public void setActivo(boolean isActivo) {
        this.isActivo = isActivo;
    }

    @JsonIgnore
    public boolean isSincronizado() {
        return isSincronizado;
    }

    public void setSincronizado(boolean isSincronizado) {
        this.isSincronizado = isSincronizado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @JsonIgnore
    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public int getIdTema() {
        return idTema;
    }

    public void setIdTema(int idTema) {
        this.idTema = idTema;
    }

}
