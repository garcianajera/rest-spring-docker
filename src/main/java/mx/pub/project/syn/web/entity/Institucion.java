package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "im3_institucion")
public class Institucion implements Serializable {

    private static final long serialVersionUID = -5392886782420959388L;

    @Id
    @Column
    private int idTema;

    @Column
    private String clave;

    @Column(name = "activo")
    private boolean isActivo;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_subsegmento", nullable = false)
    private Subsegmento subsegmentoEntity;

    @Transient
    private int idSubsegmento;

    @Transient
    private String tipo;

    @Transient
    private String subsegmento;

    @OneToOne
    @JoinColumn(name = "id_tema")
    private Tema tema;

    @JsonIgnore
    @Column
    private Date fechaActualizacion;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public boolean isActivo() {
        return isActivo;
    }

    public void setActivo(boolean isActivo) {
        this.isActivo = isActivo;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public int getIdTema() {
        return idTema;
    }

    public void setIdTema(int idTema) {
        this.idTema = idTema;
    }

    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }

    public String getSubsegmento() {
        return subsegmentoEntity.getDescripcion();
    }

    public void setSubsegmento(String subsegmento) {
        this.subsegmento = subsegmento;
    }

    public int getIdSubsegmento() {
        return subsegmentoEntity.getIdSubsegmento();
    }

    public void setIdSubsegmento(int idSubsegmento) {
        this.idSubsegmento = idSubsegmento;
    }

    public String getTipo() {
        return subsegmentoEntity.getTipo().getDescripcion();
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Subsegmento getSubsegmentoEntity() {
        return subsegmentoEntity;
    }

    public void setSubsegmentoEntity(Subsegmento subsegmentoEntity) {
        this.subsegmentoEntity = subsegmentoEntity;
    }
}
