package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Noticia;
import mx.pub.project.syn.web.entity.NoticiaTema;
import mx.pub.project.syn.web.entity.NoticiaTemaPk;
import mx.pub.project.syn.web.entity.Tema;
import mx.pub.project.syn.web.repository.NoticiaTemaRepository;
import mx.pub.project.syn.web.service.NoticiaTemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



@Service
public class NoticiaTemaServiceImpl implements NoticiaTemaService {

    @Autowired
    private NoticiaTemaRepository noticiaTemaRepository;

    @Override
    public NoticiaTema crear(NoticiaTema entity) {
        entity.setFechaCreacion(new Date());
        return actualizar(entity);
    }

    @Override
    public NoticiaTema actualizar(NoticiaTema entity) {
        entity.setFechaActualizacion(new Date());
        return noticiaTemaRepository.save(entity);
    }

    @Override
    public List<NoticiaTema> list() {
        List<NoticiaTema> list = new ArrayList<>();
        Iterable<NoticiaTema> iter = noticiaTemaRepository.findAll();
        iter.forEach(list::add);
        return list;
    }

    @Override
    public NoticiaTema getById(int idNoticia, int idTema) {
        Noticia noticia = new Noticia();
        noticia.setIdNoticia(idNoticia);
        Tema tema = new Tema();
        tema.setIdTema(idTema);
        return noticiaTemaRepository.findOne(new NoticiaTemaPk(noticia, tema));
    }

}
