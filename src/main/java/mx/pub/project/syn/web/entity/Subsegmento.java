package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity(name = "im3_subsegmento")
public class Subsegmento implements Serializable {

    private static final long serialVersionUID = 3337157433195370887L;

    @javax.persistence.Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSubsegmento;

    @Transient
    private int idTipo;

    @Column
    private String descripcion;

    @Column
    private String clave;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_tipo", nullable = false)
    private Tipo tipo;

    @JsonIgnore
    @OneToMany(mappedBy = "subsegmentoEntity", fetch = FetchType.EAGER)
    private List<Institucion> instituciones;

    @JsonIgnore
    @Column
    private Date fechaActualizacion;

    public int getIdSubsegmento() {
        return idSubsegmento;
    }

    public void setIdSubsegmento(int idSubsegmento) {
        this.idSubsegmento = idSubsegmento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public int getIdTipo() {
        return tipo.getIdTipo();
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public List<Institucion> getInstituciones() {
        return instituciones;
    }

    public void setInstituciones(List<Institucion> instituciones) {
        this.instituciones = instituciones;
    }

}
