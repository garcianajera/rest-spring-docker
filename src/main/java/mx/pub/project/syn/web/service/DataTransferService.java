package mx.

public interface DataTransferService {

    public void syncData();

    public void loadNoticias();

}
