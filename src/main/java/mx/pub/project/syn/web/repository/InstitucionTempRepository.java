package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.InstitucionTemp;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface InstitucionTempRepository extends CrudRepository<InstitucionTemp, Integer> {

    List<InstitucionTemp> findAllByFechaActualizacionAfter(Date fechaActualizacion);
}
