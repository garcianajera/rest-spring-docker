package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Tema;
import mx.pub.project.syn.web.repository.TemaRepository;
import mx.pub.project.syn.web.service.TemaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;



@Service
public class TemaServiceImpl implements TemaService {
    private final static Logger _logger = LoggerFactory.getLogger(TemaServiceImpl.class);

    @Autowired
    private TemaRepository temaRepository;

    @Override
    public void activar(int id) {
        Tema tema = temaRepository.findOne(id);
        if (tema == null) {
            throw new RuntimeException("tema no existe");
        }
        tema.setActivo(true);
        saveOrUpdate(tema);

        _logger.info("Activando Tema " + tema.getNombre());
        _logger.info("Usuario: " + getUser());
    }

    @Override
    public void desactivar(int id) {
        Tema tema = temaRepository.findOne(id);
        if (tema == null) {
            throw new RuntimeException("tema no existe");
        }
        tema.setActivo(false);
        saveOrUpdate(tema);
        _logger.info("Desactivando Tema " + tema.getNombre());
        _logger.info("Usuario: " + getUser());
    }

    @Override
    public Tema crear(Tema entity) {
        entity.setFechaCreacion(new Date());
        _logger.info("Creando Tema " + entity.getNombre());
        _logger.info("Usuario: " + getUser());
        return saveOrUpdate(entity);
    }

    @Override
    public Tema actualizar(Tema entity) {
        Tema temaFromDb = temaRepository.findOne(entity.getIdTema());
        if (temaFromDb == null) {
            throw new RuntimeException("Tema no existe: " + entity.getIdTema());
        }
        if (entity.getNombre() != null) {
            temaFromDb.setNombre(entity.getNombre());
        }
        temaFromDb.setActivo(entity.isActivo());
        _logger.info("Actualizando Tema " + temaFromDb.getNombre());
        _logger.info("Usuario: " + getUser());
        return saveOrUpdate(temaFromDb);
    }

    @Override
    public List<Tema> list() {
        return temaRepository.findByInstitucionIsNull();
    }

    @Override
    public Tema getById(int id) {
        return temaRepository.findOne(id);
    }

    private Tema saveOrUpdate(Tema entity) {
        entity.setFechaActualizacion(new Date());
        entity.setSincronizado(false);
        return temaRepository.save(entity);
    }


    private String getUser() {
        return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
