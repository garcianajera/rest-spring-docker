package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Phrase;
import mx.pub.project.syn.web.repository.PhraseRepository;
import mx.pub.project.syn.web.service.PhraseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



@Service
public class PhraseServiceImpl implements PhraseService {
    private final static Logger _logger = LoggerFactory.getLogger(PhraseServiceImpl.class);

    @Autowired
    private PhraseRepository PhraseRepository;

    @Override
    public void activarPhrase(int idPhrase) {
        Phrase Phrase = PhraseRepository.findOne(idPhrase);
        if (Phrase == null) {
            throw new RuntimeException("Phrase no existe");
        }
        Phrase.setActivo(true);
        saveOrUpdate(Phrase);
        _logger.info("Activando Phrase " + Phrase.getPhrase());
        _logger.info("Usuario: " + getUser());
    }

    @Override
    public void desactivarPhrase(int idPhrase) {
        Phrase Phrase = PhraseRepository.findOne(idPhrase);
        if (Phrase == null) {
            throw new RuntimeException("Phrase no existe");
        }
        Phrase.setActivo(false);
        saveOrUpdate(Phrase);
        _logger.info("Desactivando Phrase " + Phrase.getPhrase());
        _logger.info("Usuario: " + getUser());

    }

    @Override
    public Phrase crearPhrase(Phrase Phrase) {
        Phrase.setFechaCreacion(new Date());
        _logger.info("Creando Phrase " + Phrase.getPhrase());
        _logger.info("Usuario: " + getUser());

        return saveOrUpdate(Phrase);
    }

    @Override
    public Phrase actualizarPhrase(Phrase Phrase) {
        Phrase PhraseFromDb = PhraseRepository.findOne(Phrase.getIdPhrase());

        if (PhraseFromDb == null) {
            throw new RuntimeException("Phrase no existe: " + Phrase.getIdPhrase());
        }
        PhraseFromDb.setPhrase(Phrase.getPhrase());
        PhraseFromDb.setActivo(Phrase.isActivo());
        PhraseFromDb.setPeso(Phrase.getPeso());
        _logger.info("Actualizando Phrase " + PhraseFromDb.getPhrase());
        _logger.info("Usuario: " + getUser());

        return saveOrUpdate(PhraseFromDb);
    }

    @Override
    public List<Phrase> listPhrases() {
        List<Phrase> Phrases = new ArrayList<>();
        Iterable<Phrase> PhrasesIterable = PhraseRepository.findAll();
        PhrasesIterable.forEach(Phrases::add);
        return Phrases;
    }

    @Override
    public Phrase getById(int id) {
        return PhraseRepository.findOne(id);
    }

    @Override
    public List<Phrase> listPhrases(int idTema) {
        return PhraseRepository.findByIdTema(idTema);
    }

    private Phrase saveOrUpdate(Phrase entity) {
        entity.setFechaActualizacion(new Date());
        entity.setSincronizado(false);
        return PhraseRepository.save(entity);
    }


    private String getUser() {
        return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
