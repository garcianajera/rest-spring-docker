package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Tipo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TipoRepository extends CrudRepository<Tipo, Integer> {

    Tipo findByClave(String clave);

}
