package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.Medio;

import java.util.List;



public interface MedioService {
    Medio crear(Medio entity);

    Medio actualizar(Medio entity);

    List<Medio> list();

    Medio getById(int id);

    void activar(int id);

    void desactivar(int id);

}
