package mx.pub.project.syn.web;

public class SecurityConstants {

    public static final String TENANT_ATTRIBUTE = "tenant";
    public static final String SECRET = "SecKeyZicral";
    public static final long EXPIRATION_TIME = 28800000; // 8 horas
    public static final String TOKEN_PREFIX = "bearer ";
    public static final String HEADER_STRING = "Authorization";

}
