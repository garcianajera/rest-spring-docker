package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity(name = "im3_noticia")
public class Noticia implements Serializable {

    private static final long serialVersionUID = 2899127840311733204L;

    @javax.persistence.Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idNoticia;

    @Column
    private String titulo;

    @Column
    private String contenido;

    @Column
    private int sentimiento;

    @Column
    private float sentimientoScore;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "America/Mexico_City")
    private Date fechaPublicacion;

    @Column
    private String url;

    @Column
    private String palabrasClaveBusqueda;

    @Transient
    private int idMedio;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_medio", nullable = false)
    private Medio medioEntity;

    @Column
    private String resumen;

    @Column
    private int sentimientoUsuario;

    @Column
    private int relevanciaUsuario;

    @Column
    private int relevanciaCalculada;

    @Column
    private String estatusUsuario;

    @Column
    private String ubicacion;

    @Column
    private float relevanciaScore;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "noticiaTemaPk.noticia")
    private Set<NoticiaTema> temas;

    @Transient
    private String medio;

    @JsonIgnore
    @Column(name = "sincronizado")
    private boolean isSincronizado;

    @Column
    private Date fechaCreacion;

    @JsonIgnore
    @Column
    private Date fechaActualizacion;

    public int getIdNoticia() {
        return idNoticia;
    }

    public void setIdNoticia(int idNoticia) {
        this.idNoticia = idNoticia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public int getSentimiento() {
        return sentimiento;
    }

    public void setSentimiento(int sentimiento) {
        this.sentimiento = sentimiento;
    }

    public float getSentimientoScore() {
        return sentimientoScore;
    }

    public void setSentimientoScore(float sentimientoScore) {
        this.sentimientoScore = sentimientoScore;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPalabrasClaveBusqueda() {
        return palabrasClaveBusqueda;
    }

    public void setPalabrasClaveBusqueda(String palabrasClaveBusqueda) {
        this.palabrasClaveBusqueda = palabrasClaveBusqueda;
    }

    public int getIdMedio() {
        return idMedio;
    }

    public void setIdMedio(int idMedio) {
        this.idMedio = idMedio;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public int getSentimientoUsuario() {
        return sentimientoUsuario;
    }

    public void setSentimientoUsuario(int sentimientoUsuario) {
        this.sentimientoUsuario = sentimientoUsuario;
    }

    public int getRelevanciaUsuario() {
        return relevanciaUsuario;
    }

    public void setRelevanciaUsuario(int relevanciaUsuario) {
        this.relevanciaUsuario = relevanciaUsuario;
    }

    public int getRelevanciaCalculada() {
        return relevanciaCalculada;
    }

    public void setRelevanciaCalculada(int relevanciaCalculada) {
        this.relevanciaCalculada = relevanciaCalculada;
    }

    public String getEstatusUsuario() {
        return estatusUsuario;
    }

    public void setEstatusUsuario(String estatusUsuario) {
        this.estatusUsuario = estatusUsuario;
    }

    @JsonIgnore
    public boolean isSincronizado() {
        return isSincronizado;
    }

    public void setSincronizado(boolean isSincronizado) {
        this.isSincronizado = isSincronizado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public float getRelevanciaScore() {
        return relevanciaScore;
    }

    public void setRelevanciaScore(float relevanciaScore) {
        this.relevanciaScore = relevanciaScore;
    }

    public String getMedio() {
        return medioEntity.getNombre();
    }

    public void setMedio(String medio) {
        this.medio = medio;
    }

    public Set<NoticiaTema> getTemas() {
        return temas;
    }

    public void setTemas(Set<NoticiaTema> temas) {
        this.temas = temas;
    }

    public Medio getMedioEntity() {
        return medioEntity;
    }

    public void setMedioEntity(Medio medioEntity) {
        this.medioEntity = medioEntity;
    }


}
