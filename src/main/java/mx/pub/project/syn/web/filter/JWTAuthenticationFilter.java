package mx.pub.project.syn.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import mx.pub.project.syn.web.SecurityConstants;
import mx.pub.project.syn.web.entity.AppUser;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

pub.project.syn.web.filter;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final static Logger _logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

    private final AuthenticationManager authenticationManager;

    private ObjectMapper mapper = new ObjectMapper();

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException {

        AppUser creds = null;
        Exception exception = null;
        for (int i = 1; i <= 2; i++) {
            try {
                creds = mapper.readValue(req.getInputStream(), AppUser.class);
                String user = decode(creds.getUsername());

                _logger.info("Autenticando usuario:" + user);

                return authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(user, decode(creds.getPassword()), null));
            } catch (IOException e) {
                logger.error("Error, intento: " + i, e);
                exception = e;
            }
        }
        throw new RuntimeException(exception);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        String token = Jwts.builder().setSubject(((LdapUserDetailsImpl) auth.getPrincipal()).getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET.getBytes()).compact();

        res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
    }

    private String decode(String code) {
        if (code == null)
            return "";

        byte[] byteArray = Base64.decodeBase64(code.getBytes());

        return new String(byteArray);
    }
}
