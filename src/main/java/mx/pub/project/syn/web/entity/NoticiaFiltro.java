package mx.pub.project.syn.web.entity;

public class NoticiaFiltro {

    private Integer periodo;

    private Integer trimestre;

    private Integer mes;

    private Integer idTipo;

    private Integer idSubsegmento;

    private Integer idInstitucion;

    private Integer idTema;

    private Integer idMedio;

    private Integer relevancia;

    private Integer sentimiento;

    private String estatus;

    private String buscar;

    private Integer pagina;

    private String ordena;

    private String ordenaTipo;

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public Integer getTrimestre() {
        return trimestre;
    }

    public void setTrimestre(Integer trimestre) {
        this.trimestre = trimestre;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public Integer getIdSubsegmento() {
        return idSubsegmento;
    }

    public void setIdSubsegmento(Integer idSubsegmento) {
        this.idSubsegmento = idSubsegmento;
    }

    public Integer getIdInstitucion() {
        return idInstitucion;
    }

    public void setIdInstitucion(Integer idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public Integer getIdTema() {
        return idTema;
    }

    public void setIdTema(Integer idTema) {
        this.idTema = idTema;
    }

    public Integer getRelevancia() {
        return relevancia;
    }

    public void setRelevancia(Integer relevancia) {
        this.relevancia = relevancia;
    }

    public Integer getSentimiento() {
        return sentimiento;
    }

    public void setSentimiento(Integer sentimiento) {
        this.sentimiento = sentimiento;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getBuscar() {
        return buscar;
    }

    public void setBuscar(String buscar) {
        this.buscar = buscar;
    }

    public Integer getPagina() {
        return pagina;
    }

    public void setPagina(Integer pagina) {
        this.pagina = pagina;
    }

    public Integer getIdMedio() {
        return idMedio;
    }

    public void setIdMedio(Integer idMedio) {
        this.idMedio = idMedio;
    }

    public String getOrdena() {
        return ordena;
    }

    public void setOrdena(String ordena) {
        this.ordena = ordena;
    }

    public String getOrdenaTipo() {
        return ordenaTipo;
    }

    public void setOrdenaTipo(String ordenaTipo) {
        this.ordenaTipo = ordenaTipo;
    }


}
