package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Noticia;
import mx.pub.project.syn.web.entity.NoticiaFiltro;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Repository
public class NoticiaRepositoryImpl implements NoticiaCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public List<Noticia> listPorFiltro(NoticiaFiltro noticiaFiltro, int inicio, int resultados, List<Integer> idTemas)
            throws Exception {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Noticia> query = builder.createQuery(Noticia.class);
        Root<Noticia> from = query.from(Noticia.class);
        query.select(from);
        Predicate predicate = buildPredicate(noticiaFiltro, builder, from, idTemas);
        query.where(predicate);

        ordenaNoticias(noticiaFiltro, builder, query, from);

        TypedQuery<Noticia> typedQuery = entityManager.createQuery(query);
        typedQuery.setFirstResult(inicio);
        typedQuery.setMaxResults(resultados);
        return typedQuery.getResultList();
    }

    private void ordenaNoticias(NoticiaFiltro noticiaFiltro, CriteriaBuilder builder, CriteriaQuery<Noticia> query,
                                Root<Noticia> from) {

        if (StringUtils.isBlank(noticiaFiltro.getOrdena()) || noticiaFiltro.getOrdena().equalsIgnoreCase("fecha")) {
            if ("asc".equalsIgnoreCase(noticiaFiltro.getOrdenaTipo())) {
                query.orderBy(builder.asc(from.get("fechaPublicacion")));
            } else {
                query.orderBy(builder.desc(from.get("fechaPublicacion")), builder.desc(from.get("relevanciaUsuario")));
            }
        } else if (noticiaFiltro.getOrdena().equalsIgnoreCase("encabezado")) {
            if ("asc".equalsIgnoreCase(noticiaFiltro.getOrdenaTipo())) {
                query.orderBy(builder.asc(from.get("titulo")));
            } else {
                query.orderBy(builder.desc(from.get("titulo")));
            }
        } else if (noticiaFiltro.getOrdena().equalsIgnoreCase("noticia")) {
            if ("asc".equalsIgnoreCase(noticiaFiltro.getOrdenaTipo())) {
                query.orderBy(builder.asc(from.get("contenido")));
            } else {
                query.orderBy(builder.desc(from.get("contenido")));
            }
        } else if (noticiaFiltro.getOrdena().equalsIgnoreCase("fuente")) {
            if ("asc".equalsIgnoreCase(noticiaFiltro.getOrdenaTipo())) {
                query.orderBy(builder.asc(from.get("medioEntity").get("nombre")));
            } else {
                query.orderBy(builder.desc(from.get("medioEntity").get("nombre")));
            }
        } else if (noticiaFiltro.getOrdena().equalsIgnoreCase("relevancia")) {
            if ("asc".equalsIgnoreCase(noticiaFiltro.getOrdenaTipo())) {
                query.orderBy(builder.asc(from.get("relevanciaUsuario")));
            } else {
                query.orderBy(builder.desc(from.get("relevanciaUsuario")));
            }
        } else if (noticiaFiltro.getOrdena().equalsIgnoreCase("sentimiento")) {
            if ("asc".equalsIgnoreCase(noticiaFiltro.getOrdenaTipo())) {
                query.orderBy(builder.asc(from.get("sentimientoUsuario")));
            } else {
                query.orderBy(builder.desc(from.get("sentimientoUsuario")));
            }
        } else if (noticiaFiltro.getOrdena().equalsIgnoreCase("estado")) {
            if ("asc".equalsIgnoreCase(noticiaFiltro.getOrdenaTipo())) {
                query.orderBy(builder.asc(from.get("estatusUsuario")));
            } else {
                query.orderBy(builder.desc(from.get("estatusUsuario")));
            }
        }
    }

    @Override
    public Long listPorFiltroTotal(NoticiaFiltro noticiaFiltro, List<Integer> idTemas) throws Exception {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);
        Root<Noticia> fromCount = countQuery.from(Noticia.class);
        Predicate predicateCount = buildPredicate(noticiaFiltro, builder, fromCount, idTemas);

        countQuery.select(builder.count(fromCount));
        countQuery.where(predicateCount);
        return entityManager.createQuery(countQuery).getSingleResult();
    }

    private Predicate buildPredicate(NoticiaFiltro noticiaFiltro, CriteriaBuilder builder, Root<Noticia> from,
                                     List<Integer> idTemas) throws Exception, ParseException {
        Predicate predicate;

        if (noticiaFiltro.getPeriodo() == null) {
            predicate = periodo(builder, from);

        } else {
            predicate = periodo(noticiaFiltro.getPeriodo(), builder, from);
        }

        if (noticiaFiltro.getTrimestre() != null) {
            predicate = addTrimestre(predicate, noticiaFiltro.getTrimestre(), noticiaFiltro.getPeriodo(), builder,
                    from);
        }

        if (noticiaFiltro.getMes() != null) {
            predicate = addMes(predicate, noticiaFiltro.getMes(), noticiaFiltro.getPeriodo(), builder, from);
        }

        if (noticiaFiltro.getEstatus() != null) {
            predicate = addEstatus(predicate, noticiaFiltro.getEstatus(), builder, from);
        }

        if (noticiaFiltro.getRelevancia() != null) {
            predicate = addRelevancia(predicate, noticiaFiltro.getRelevancia(), builder, from);
        }

        if (noticiaFiltro.getSentimiento() != null) {
            predicate = addSentimiento(predicate, noticiaFiltro.getSentimiento(), builder, from);
        }

        if (idTemas != null && !idTemas.isEmpty()) {
            predicate = addTema(predicate, idTemas, builder, from);
        }

        if (noticiaFiltro.getBuscar() != null) {
            predicate = addBusquedaPalabra(predicate, noticiaFiltro.getBuscar(), builder, from);
        }

        return predicate;
    }

    private Predicate addBusquedaPalabra(Predicate predicate, String buscar, CriteriaBuilder builder,
                                         Root<Noticia> from) {
        return builder.and(predicate, builder.like(from.get("contenido"), "%" + buscar + "%"));
    }

    private Predicate addTema(Predicate predicate, List<Integer> idTemas, CriteriaBuilder builder, Root<Noticia> from) {
        Join<Object, Object> temas = from.join("temas").join("noticiaTemaPk").join("tema");
        return builder.and(predicate, temas.get("idTema").in(idTemas));
    }

    private Predicate addSentimiento(Predicate predicate, Integer sentimiento, CriteriaBuilder builder,
                                     Root<Noticia> from) {
        return builder.and(predicate, builder.equal(from.get("sentimientoUsuario"), sentimiento));
    }

    private Predicate addRelevancia(Predicate predicate, Integer relevancia, CriteriaBuilder builder,
                                    Root<Noticia> from) {
        return builder.and(predicate, builder.equal(from.get("relevanciaUsuario"), relevancia));
    }

    private Predicate addEstatus(Predicate predicate, String estatus, CriteriaBuilder builder, Root<Noticia> from) {
        return builder.and(predicate, builder.equal(from.get("estatusUsuario"), estatus.toUpperCase()));
    }

    private Predicate addMes(Predicate predicate, Integer mes, Integer periodo, CriteriaBuilder builder,
                             Root<Noticia> from) throws ParseException {
        String fromString = "01/01/" + periodo;
        String toString = "31/12/" + periodo;

        if (mes == 1) {
            fromString = "01/01/" + periodo;
            toString = "31/01/" + periodo;
        } else if (mes == 2) {
            fromString = "01/02/" + periodo;
            if ((periodo % 4 == 0) && ((periodo % 100 != 0) || (periodo % 400 == 0))) {
                toString = "29/02/" + periodo;
            } else {
                toString = "28/02/" + periodo;
            }
        } else if (mes == 3) {
            fromString = "01/03/" + periodo;
            toString = "31/03/" + periodo;
        } else if (mes == 4) {
            fromString = "01/04/" + periodo;
            toString = "30/04/" + periodo;
        } else if (mes == 5) {
            fromString = "01/05/" + periodo;
            toString = "31/05/" + periodo;
        } else if (mes == 6) {
            fromString = "01/06/" + periodo;
            toString = "30/06/" + periodo;
        } else if (mes == 7) {
            fromString = "01/07/" + periodo;
            toString = "31/07/" + periodo;
        } else if (mes == 8) {
            fromString = "01/08/" + periodo;
            toString = "31/08/" + periodo;
        } else if (mes == 9) {
            fromString = "01/09/" + periodo;
            toString = "30/09/" + periodo;
        } else if (mes == 10) {
            fromString = "01/10/" + periodo;
            toString = "31/10/" + periodo;
        } else if (mes == 11) {
            fromString = "01/11/" + periodo;
            toString = "30/11/" + periodo;
        } else if (mes == 12) {
            fromString = "01/12/" + periodo;
            toString = "31/12/" + periodo;
        }

        Date fromDate = dateFormat.parse(fromString);
        Date toDate = dateFormat.parse(toString);

        return builder.and(predicate, builder.between(from.get("fechaPublicacion"), fromDate, toDate));
    }

    Predicate periodo(Integer periodo, CriteriaBuilder builder, Root<Noticia> from) throws Exception {

        Date fromDate = dateFormat.parse("01/01/" + periodo);
        Date toDate = dateFormat.parse("31/12/" + periodo);

        return builder.between(from.get("fechaPublicacion"), fromDate, toDate);
    }

    Predicate periodo(CriteriaBuilder builder, Root<Noticia> from) throws Exception {
        Date toDate = dateFormat.parse("31/12/" + Calendar.getInstance().get(Calendar.YEAR));
        return builder.lessThan(from.get("fechaPublicacion"), toDate);
    }

    Predicate addTrimestre(Predicate predicate, Integer trimestre, Integer periodo, CriteriaBuilder builder,
                           Root<Noticia> from) throws Exception {

        String fromString = "01/01/" + periodo;
        String toString = "31/12/" + periodo;

        if (trimestre == 1) {
            fromString = "01/01/" + periodo;
            toString = "31/03/" + periodo;
        } else if (trimestre == 2) {
            fromString = "01/04/" + periodo;
            toString = "30/06/" + periodo;
        } else if (trimestre == 3) {
            fromString = "01/07/" + periodo;
            toString = "30/09/" + periodo;
        } else if (trimestre == 4) {
            fromString = "01/10/" + periodo;
            toString = "31/12/" + periodo;
        }

        Date fromDate = dateFormat.parse(fromString);
        Date toDate = dateFormat.parse(toString);

        return builder.and(predicate, builder.between(from.get("fechaPublicacion"), fromDate, toDate));
    }

}
