package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Noticia;
import mx.pub.project.syn.web.entity.NoticiaFiltro;

import java.util.List;


public interface NoticiaCustomRepository {

    List<Noticia> listPorFiltro(NoticiaFiltro noticiaFiltro, int inicio, int resultados, List<Integer> idTemas) throws Exception;

    Long listPorFiltroTotal(NoticiaFiltro noticiaFiltro, List<Integer> idTemas) throws Exception;

}
