package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Phrase;
import mx.pub.project.syn.web.entity.Medio;
import mx.pub.project.syn.web.entity.Noticia;
import mx.pub.project.syn.web.entity.Tema;
import mx.pub.project.syn.web.repository.PhraseRepository;
import mx.pub.project.syn.web.repository.MedioRepository;
import mx.pub.project.syn.web.repository.NoticiaRepository;
import mx.pub.project.syn.web.repository.TemaRepository;
import mx.pub.project.syn.web.service.DataTransferService;
import mx.pub.project.syn.web.service.NoticiaService;
import mx.pub.project.syn.web.service.SincronizaInstitucionesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;



@Service
public class DataTransferServiceImpl implements DataTransferService {
    private final static Logger _logger = LoggerFactory.getLogger(DataTransferServiceImpl.class);

    @Value("${aws.url}")
    private String url;

    @Value("${habilitar.tareas.programadas:true}")
    private boolean tareaHabilitada;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TemaRepository temaRepository;

    @Autowired
    private PhraseRepository PhraseRepository;

    @Autowired
    private MedioRepository medioRepository;

    @Autowired
    private NoticiaRepository noticiaRepository;

    @Autowired
    private NoticiaService noticiaService;

    @Autowired
    private SincronizaInstitucionesService sincronizaInstitucionesService;

    @Override
    @Scheduled(cron = "0 30 7 * * ?")
    public void syncData() {
        if (!tareaHabilitada) {
            _logger.info("configuracion habilitar.tareas.programadas no habilitada");
            return;
        }
        // iniciando tarea
        try {
            restTemplate.getForEntity(url + "/api/task/config/iniciar", String.class);
        } catch (Exception e) {
            _logger.info(
                    "No se pudo iniciar tarea of sincronizacion, es posible que el proceso se encuntre en ejecucion.");
            return;
        }

        _logger.info("Iniciando tarea of sincronizacion");
        sincronizaInstitucionesService.sincronizaInstituciones();
        _logger.info("Sincronizando temas");
        sendTemas();
        _logger.info("Sincronizando Phrases");
        sendPhrases();
        _logger.info("Sincronizando medios");
        sendMedios();
        _logger.info("Sincronizando noticias");
        sendNoticias();

        _logger.info("Finaliza tarea of sincronizacion");

        terminar(url + "/api/task/config/finalizar");
    }

    @Override
    @Scheduled(cron = "0 45 9,10,11 * * ?")
    public void loadNoticias() {
        if (!tareaHabilitada) {
            _logger.info("configuracion habilitar.tareas.programadas no habilitada");
            return;
        }
        // iniciando tarea
        try {
            restTemplate.getForEntity(url + "/api/task/noticias/iniciar", String.class);
        } catch (Exception e) {
            _logger.info(
                    "No se pudo iniciar tarea of sincronizacion, es posible que el proceso se encuntre en ejecucion.");
            return;
        }
        _logger.info("Iniciando tarea of carga of noticias");

        _logger.info("Descarga noticias");
        cargaNoticias();
        _logger.info("Finaliza tarea of carga");

        terminar(url + "/api/task/noticias/finalizar");
    }

    private void cargaNoticias() {
        ResponseEntity<List<Noticia>> response = restTemplate.exchange(url + "/api/noticia/descargar", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<Noticia>>() {
                });
        List<Noticia> noticias = response.getBody();
        _logger.info("Get noticias : " + noticias.size());
        if (noticias != null) {
            for (Noticia noticia : noticias) {
                int id = noticia.getIdNoticia();
                _logger.info("cargando noticia: " + id);
                try {
                    noticiaService.cargar(noticia);
                    noticiaProcesada(id);
                } catch (Exception e) {
                    _logger.error("error cargando noticia", e);
                }
            }
        }
    }

    private void noticiaProcesada(int id) {
        try {
            restTemplate.getForEntity(url + "/api/noticia/" + id + "/procesada", String.class);
        } catch (Exception e) {
            _logger.info("No se pudo marcar noticia como procesada." + id);
            return;
        }
    }

    private void terminar(String urlTask) {
        try {
            restTemplate.getForEntity(urlTask, String.class);
        } catch (Exception e) {
            _logger.info("No se pudo marcar tarea como procesada.", e.getMessage());
            return;
        }
    }

    private void sendNoticias() {
        List<Noticia> noticias = noticiaRepository.findByIsSincronizadoIsFalse();
        _logger.info("noticias no sincronizados:" + noticias.size());
        noticias.forEach(noticia -> {
            noticia.setIdMedio(noticia.getMedioEntity().getIdMedio());
            ResponseEntity<String> response = restTemplate.postForEntity(url + "/api/noticia/sincronizar", noticia,
                    String.class);
            if (response.getStatusCodeValue() == 200) {
                noticia.setSincronizado(true);
                noticiaRepository.save(noticia);
            } else {
                _logger.info(" error al sincronizar:" + response.getStatusCodeValue());
            }
        });
    }

    private void sendTemas() {
        List<Tema> temas = temaRepository.findByIsSincronizadoIsFalse();
        _logger.info("temas no sincronizados:" + temas.size());
        temas.forEach(tema -> {
            ResponseEntity<String> response = restTemplate.postForEntity(url + "/api/tema/sincronizar", tema,
                    String.class);
            if (response.getStatusCodeValue() == 200) {
                tema.setSincronizado(true);
                temaRepository.save(tema);
            } else {
                _logger.info(" error al sincronizar:" + response.getStatusCodeValue());
            }
        });
    }

    private void sendPhrases() {
        List<Phrase> Phrases = PhraseRepository.findByIsSincronizadoIsFalse();
        _logger.info("Phrases no sincronizados:" + Phrases.size());
        Phrases.forEach(Phrase -> {
            ResponseEntity<String> response = restTemplate.postForEntity(url + "/api/Phrase/sincronizar", Phrase,
                    String.class);
            if (response.getStatusCodeValue() == 200) {
                Phrase.setSincronizado(true);
                PhraseRepository.save(Phrase);
            } else {
                _logger.info(" error al sincronizar:" + response.getStatusCodeValue());
            }
        });
    }

    private void sendMedios() {
        List<Medio> medios = medioRepository.findByIsSincronizadoIsFalse();
        _logger.info("medios no sincronizados:" + medios.size());
        medios.forEach(medio -> {
            ResponseEntity<String> response = restTemplate.postForEntity(url + "/api/medio/sincronizar", medio,
                    String.class);
            if (response.getStatusCodeValue() == 200) {
                medio.setSincronizado(true);
                medioRepository.save(medio);
            } else {
                _logger.info(" error al sincronizar:" + response.getStatusCodeValue());
            }
        });
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
