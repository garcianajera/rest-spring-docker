package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity(name = "im3_tipo")
public class Tipo implements Serializable {

    private static final long serialVersionUID = 7023543573686022035L;
    @OneToMany(mappedBy = "tipo", fetch = FetchType.EAGER)
    @JsonIgnore
    List<Subsegmento> subsegmentos;
    @javax.persistence.Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTipo;
    @Column
    private String descripcion;
    @Column
    private String clave;
    @JsonIgnore
    @Column
    private Date fechaActualizacion;

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public List<Subsegmento> getSubsegmentos() {
        return subsegmentos;
    }

    public void setSubsegmentos(List<Subsegmento> subsegmentos) {
        this.subsegmentos = subsegmentos;
    }

}
