package mx.pub.project.syn.web.controller.

import mx.pub.project.syn.web.entity.Noticia;
import mx.pub.project.syn.web.entity.NoticiaFiltro;
import mx.pub.project.syn.web.entity.NoticiaResponse;
import mx.pub.project.syn.web.service.NoticiaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/noticia")
@Validated
public class NoticiaController {

    private final static Logger _logger = LoggerFactory.getLogger(NoticiaController.class);

    @Autowired
    private NoticiaService noticiaService;

    @GetMapping("/listr")
    public List<Noticia> getlist() {
        _logger.debug("Get list of Noticia");
        return noticiaService.list();
    }

    @GetMapping("/ultimaActualizacion")
    public String getUltimaActualizacion() {
        _logger.debug("Get fecha ultima noticia");
        return noticiaService.getUltimaActualizacion();
    }

    @PostMapping("/listr")
    public NoticiaResponse getlistPorFiltro(@RequestBody NoticiaFiltro noticiaFiltro) {
        _logger.info("Get list of Noticia");
        Date inicio = new Date();
        NoticiaResponse list = noticiaService.list(noticiaFiltro);
        _logger.info("Tiempo en busqueda of noticias (ms)" + (new Date().getTime() - inicio.getTime()));
        return list;
    }

    @PostMapping("/crear")
    public void crear(@RequestBody Noticia entity) {
        _logger.debug("Crea Noticia");
        noticiaService.crear(entity);
    }

    @PutMapping("/actualizar")
    public void actualizar(@RequestBody Noticia entity) {
        _logger.debug("Actualiza Noticia");
        noticiaService.actualizar(entity);
    }

    @GetMapping("/{id}")
    public Noticia getNoticia(@PathVariable(name = "id") int id) {
        return noticiaService.getById(id);
    }

}
