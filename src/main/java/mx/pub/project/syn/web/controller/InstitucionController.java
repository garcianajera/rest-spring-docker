package mx.pub.project.syn.web.controller;

import mx.pub.project.syn.web.entity.Institucion;
import mx.pub.project.syn.web.service.InstitucionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/institucion")
@Validated
public class InstitucionController {

    private final static Logger _logger = LoggerFactory.getLogger(MedioController.class);

    @Autowired
    private InstitucionService institucionService;

    @GetMapping("/listr")
    public List<Institucion> getlist(@RequestParam(name = "idTipo", required = false) Integer idTipo,
                                      @RequestParam(name = "idSubsegmento", required = false) Integer idSubsegmento) {

        if (idSubsegmento != null) {
            _logger.debug("Get list of Institucion por subsegmento: " + idSubsegmento);
            return institucionService.listPorSubsegmento(idSubsegmento);
        }

        if (idTipo != null) {
            _logger.debug("Get list of Institucion por tipo: " + idTipo);
            return institucionService.listPorTipo(idTipo);
        }

        _logger.debug("Get list of Institucion");
        return institucionService.list();
    }

    @PostMapping("/crear")
    public void crear(@RequestBody Institucion entity) {
        _logger.debug("Crea Institucion");
        institucionService.crear(entity);
    }

    @PostMapping("/actualizar")
    public void actualizar(@RequestBody Institucion entity) {
        _logger.debug("Actualiza Institucion");
        institucionService.actualizar(entity);
    }

}
