package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Meta {

    @JsonProperty("token_expires_in_hours")
    private Double tokenExpiresInHours;

    @JsonProperty("records_found")
    private Integer recordsFound;

    @JsonProperty("records_per_page")
    private Integer recordsPerPage;

    @JsonProperty("current_page")
    private Integer currentPage;

    @JsonProperty("total_pages")
    private Integer totalPages;

    public Double getTokenExpiresInHours() {
        return tokenExpiresInHours;
    }

    public void setTokenExpiresInHours(Double tokenExpiresInHours) {
        this.tokenExpiresInHours = tokenExpiresInHours;
    }

    public Integer getRecordsFound() {
        return recordsFound;
    }

    public void setRecordsFound(Integer recordsFound) {
        this.recordsFound = recordsFound;
    }

    public Integer getRecordsPerPage() {
        return recordsPerPage;
    }

    public void setRecordsPerPage(Integer recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
