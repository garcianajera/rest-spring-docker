package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Medio;
import mx.pub.project.syn.web.repository.MedioRepository;
import mx.pub.project.syn.web.service.MedioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



@Service
public class MedioServiceImpl implements MedioService {
    private final static Logger _logger = LoggerFactory.getLogger(MedioServiceImpl.class);

    @Autowired
    private MedioRepository medioRepository;

    @Override
    public Medio crear(Medio entity) {
        entity.setFechaCreacion(new Date());
        return saveOrUpdate(entity);
    }

    @Override
    public Medio actualizar(Medio entity) {
        Medio medioFromDb = medioRepository.findOne(entity.getIdMedio());

        if (medioFromDb == null) {
            throw new RuntimeException("Medio no existe: " + entity.getIdMedio());
        }
        medioFromDb.setNombre(entity.getNombre());
        medioFromDb.setPerfilTwitter(entity.getPerfilTwitter());
        medioFromDb.setPrioritario(entity.isPrioritario());
        medioFromDb.setTipo(entity.getTipo());
        medioFromDb.setActivo(entity.isActivo());

        _logger.info("Actualizando Medio " + medioFromDb.getNombre());
        _logger.info("Usuario: " + getUser());
        return saveOrUpdate(medioFromDb);
    }

    @Override
    public List<Medio> list() {
        List<Medio> list = new ArrayList<>();
        Iterable<Medio> iter = medioRepository.findAll();
        iter.forEach(list::add);
        return list;
    }

    @Override
    public Medio getById(int id) {
        return medioRepository.findOne(id);
    }

    private Medio saveOrUpdate(Medio entity) {
        entity.setFechaActualizacion(new Date());
        entity.setSincronizado(false);
        return medioRepository.save(entity);
    }

    @Override
    public void activar(int id) {
        Medio medio = medioRepository.findOne(id);
        if (medio == null) {
            throw new RuntimeException("medio no existe");
        }
        medio.setActivo(true);
        saveOrUpdate(medio);

        _logger.info("Activando Medio " + medio.getNombre());
        _logger.info("Usuario: " + getUser());
    }

    @Override
    public void desactivar(int id) {
        Medio medio = medioRepository.findOne(id);
        if (medio == null) {
            throw new RuntimeException("medio no existe");
        }
        medio.setActivo(false);
        saveOrUpdate(medio);
        _logger.info("Desactivando Medio " + medio.getNombre());
        _logger.info("Usuario: " + getUser());
    }

    private String getUser() {
        return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
