package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Tipo;
import mx.pub.project.syn.web.repository.TipoRepository;
import mx.pub.project.syn.web.service.TipoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



@Service
public class TipoServiceImpl implements TipoService {
    private final static Logger _logger = LoggerFactory.getLogger(TipoServiceImpl.class);

    @Autowired
    private TipoRepository tipoRepository;

    @Override
    public Tipo crear(Tipo entity) {
        return actualizar(entity);
    }

    @Override
    public Tipo actualizar(Tipo entity) {
        entity.setFechaActualizacion(new Date());

        _logger.info("Actualizando Tipo " + entity.getDescripcion());
        _logger.info("Usuario: " + getUser());
        return tipoRepository.save(entity);
    }

    @Override
    public List<Tipo> list() {
        List<Tipo> list = new ArrayList<>();
        Iterable<Tipo> iter = tipoRepository.findAll();
        iter.forEach(list::add);
        return list;
    }

    @Override
    public Tipo getById(int id) {
        return tipoRepository.findOne(id);
    }

    private String getUser() {
        return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
