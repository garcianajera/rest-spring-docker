package mx.pub.project.syn.web.controller.

import mx.pub.project.syn.web.entity.NoticiaTema;
import mx.pub.project.syn.web.service.NoticiaTemaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

pub.project.syn.web.controller;

@RestController
@RequestMapping("/api/noticiaTema")
@Validated
public class NoticiaTemaController {

    private final static Logger _logger = LoggerFactory.getLogger(NoticiaTemaController.class);

    @Autowired
    private NoticiaTemaService noticiaTemaService;

    @GetMapping("/listr")
    public List<NoticiaTema> getlist() {
        _logger.debug("Get list of NoticiaTema");
        return noticiaTemaService.list();
    }

    @PostMapping("/crear")
    public void crear(@RequestBody NoticiaTema entity) {
        _logger.debug("Crea NoticiaTema");
        noticiaTemaService.crear(entity);
    }

    @PostMapping("/actualizar")
    public void actualizar(@RequestBody NoticiaTema entity) {
        _logger.debug("Actualiza NoticiaTema");
        noticiaTemaService.actualizar(entity);
    }

}
