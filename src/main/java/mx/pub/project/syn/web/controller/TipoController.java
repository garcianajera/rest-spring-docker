package mx.pub.project.syn.web.controller.

import mx.pub.project.syn.web.entity.Tipo;
import mx.pub.project.syn.web.service.TipoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

pub.project.syn.web.controller;

@RestController
@RequestMapping("/api/tipo")
@Validated
public class TipoController {

    private final static Logger _logger = LoggerFactory.getLogger(TipoController.class);

    @Autowired
    private TipoService tipoService;

    @GetMapping("/listr")
    public List<Tipo> getlist() {
        _logger.debug("Get list of Tipo");
        return tipoService.list();
    }

    @PostMapping("/crear")
    public void crear(@RequestBody Tipo entity) {
        _logger.debug("Crea Tipo");
        tipoService.crear(entity);
    }

    @PostMapping("/actualizar")
    public void actualizar(@RequestBody Tipo entity) {
        _logger.debug("Actualiza Tipo");
        tipoService.actualizar(entity);
    }

}
