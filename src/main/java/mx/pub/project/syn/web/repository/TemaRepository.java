package mx.pub.project.syn.web.repository;
import mx.pub.project.syn.web.entity.Tema;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TemaRepository extends CrudRepository<Tema, Integer> {

    List<Tema> findByInstitucionIsNull();

    List<Tema> findByIsSincronizadoIsFalse();
}
