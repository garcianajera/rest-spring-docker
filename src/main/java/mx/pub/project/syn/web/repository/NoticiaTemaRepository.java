package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.NoticiaTema;
import mx.pub.project.syn.web.entity.NoticiaTemaPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NoticiaTemaRepository extends CrudRepository<NoticiaTema, NoticiaTemaPk> {

}
