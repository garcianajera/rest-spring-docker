package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.repository.*;
import mx.pub.project.syn.web.service.NoticiaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;



@Service
public class NoticiaServiceImpl implements NoticiaService {
    private final static Logger _logger = LoggerFactory.getLogger(NoticiaServiceImpl.class);

    private static final int RESULTADOS_PAGINA = 20;

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    @Autowired
    private NoticiaRepository noticiaRepository;

    @Autowired
    private MedioRepository medioRepository;

    @Autowired
    private InstitucionRepository institucionRepository;

    @Autowired
    private NoticiaTemaRepository noticiaTemaRepository;

    @Autowired
    private TemaRepository temaRepository;

    @Autowired
    private TipoRepository tipoRepository;

    @Override
    public Noticia crear(Noticia entity) {
        entity.setFechaCreacion(new Date());
        return actualizar(entity);
    }

    @Override
    public Noticia actualizar(Noticia entity) {
        Noticia noticiaFromDb = noticiaRepository.findOne(entity.getIdNoticia());
        if (noticiaFromDb == null) {
            throw new RuntimeException("Noticia no existe: " + entity.getIdNoticia());
        }

        noticiaFromDb.setRelevanciaUsuario(entity.getRelevanciaUsuario());
        noticiaFromDb.setSentimientoUsuario(entity.getSentimientoUsuario());
        if (entity.getEstatusUsuario() != null) {
            noticiaFromDb.setEstatusUsuario(entity.getEstatusUsuario());
        }

        if (entity.getTemas() != null) {
            actualizaTemas(noticiaFromDb, entity.getTemas());
        }
        _logger.info("Actualizando Noticia " + noticiaFromDb.getUrl());
        _logger.info("Usuario: " + getUser());
        return saveOrUpdate(noticiaFromDb, false);
    }

    private void actualizaTemas(Noticia noticiaFromDb, Set<NoticiaTema> noticiaTemaRequest) {
        for (NoticiaTema noticiaTema : noticiaTemaRequest) {
            boolean temaExistente = false;
            for (NoticiaTema noticiaTemaFromDb : noticiaFromDb.getTemas()) {
                if (noticiaTemaFromDb.getNoticiaTemaPk().getTema().getIdTema() == noticiaTema.getNoticiaTemaPk()
                        .getTema().getIdTema()) {
                    actualizaNoticiaTema(noticiaTemaFromDb, noticiaTema, false);
                    _logger.info("Actualiza noticia tema");
                    temaExistente = true;
                }
            }
            if (!temaExistente) {
                _logger.info("Agrega noticia tema");
                agregaNoticiaTema(noticiaFromDb, noticiaTema);
            }

        }

    }

    private void agregaNoticiaTema(Noticia noticiaFromDb, NoticiaTema noticiaTema) {
        int idTema = noticiaTema.getNoticiaTemaPk().getTema().getIdTema();
        Tema temaFromDb = temaRepository.findOne(idTema);
        if (temaFromDb == null) {
            throw new RuntimeException("No existe tema con id" + idTema);
        }
        noticiaTema.setNoticiaTemaPk(new NoticiaTemaPk(noticiaFromDb, temaFromDb));
        agregaNoticiaTema(noticiaTema, false);
    }

    private void agregaNoticiaTema(NoticiaTema noticiaTema, boolean isSincronizado) {
        noticiaTema.setFechaCreacion(new Date());
        saverOrUpdateNoticiaTema(noticiaTema, isSincronizado);
    }

    private void actualizaNoticiaTema(NoticiaTema noticiaTemaFromDb, NoticiaTema noticiaTema, boolean isSincronizado) {
        noticiaTemaFromDb.setEstatusUsuario(noticiaTema.getEstatusUsuario());
        saverOrUpdateNoticiaTema(noticiaTemaFromDb, isSincronizado);
    }

    private void saverOrUpdateNoticiaTema(NoticiaTema noticiaTemaFromDb, boolean isSincronizado) {
        noticiaTemaFromDb.setFechaActualizacion(new Date());
        noticiaTemaFromDb.setSincronizado(isSincronizado);
        noticiaTemaRepository.save(noticiaTemaFromDb);
    }

    @Override
    public List<Noticia> list() {
        List<Noticia> list = new ArrayList<>();
        Iterable<Noticia> iter = noticiaRepository.findAll();
        iter.forEach(list::add);
        return list;
    }

    @Override
    public Noticia getById(int id) {
        return noticiaRepository.findOne(id);
    }

    private Noticia saveOrUpdate(Noticia entity, boolean isSincronzado) {
        entity.setFechaActualizacion(new Date());
        entity.setSincronizado(isSincronzado);
        return noticiaRepository.save(entity);
    }

    @Override
    public NoticiaResponse list(NoticiaFiltro filtro) {
        try {
            List<Integer> idTemas = getIdTemas(filtro);
            int fromIndex = 0;
            Integer pagina = 0;
            Integer totalNoticias = noticiaRepository.listPorFiltroTotal(filtro, idTemas).intValue();
            Integer totalPaginas = 0;
            NoticiaResponse response = new NoticiaResponse();

            if (totalNoticias > 0) {
                totalPaginas = (int) Math.ceil((float) totalNoticias / RESULTADOS_PAGINA);
                if (filtro.getPagina() == null || filtro.getPagina() <= 0) {
                    pagina = 1;
                } else if (filtro.getPagina() > totalPaginas) {
                    pagina = totalPaginas;
                } else {
                    pagina = filtro.getPagina();
                }

                fromIndex = RESULTADOS_PAGINA * (pagina - 1);
                response.setNoticias(noticiaRepository.listPorFiltro(filtro, fromIndex, RESULTADOS_PAGINA, idTemas));

            }

            response.setPagina(pagina);
            response.setTotalNoticias(totalNoticias);
            response.setTotalPaginas(totalPaginas);

            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error al consultar noticias por filtro", e);
        }
    }

    private List<Integer> getIdTemas(NoticiaFiltro filtro) {
        List<Integer> idTemas = new ArrayList<>();
        if (filtro.getIdInstitucion() != null) {
            idTemas.add(filtro.getIdInstitucion());
        }
        if (filtro.getIdTema() != null) {
            idTemas.add(filtro.getIdTema());

        }
        if (filtro.getIdTipo() != null) {
            Tipo tipo = tipoRepository.findOne(filtro.getIdTipo());
            if (tipo != null) {
                tipo.getSubsegmentos().forEach(subsegmento -> {
                    subsegmento.getInstituciones().forEach(institutcion -> {
                        idTemas.add(institutcion.getIdTema());
                    });
                });
            }
        }
        if (filtro.getIdSubsegmento() != null) {
            List<Institucion> instituciones = institucionRepository
                    .findBySubsegmentoEntityIdSubsegmento(filtro.getIdSubsegmento());
            if (instituciones != null) {
                instituciones.forEach(institucion -> {
                    idTemas.add(institucion.getIdTema());
                });
            }
        }
        return idTemas;
    }

    @Override
    public Noticia cargar(Noticia entity) {
        Noticia noticiaFromDb = noticiaRepository.findByUrl(entity.getUrl());
        if (noticiaFromDb == null) {
            _logger.info("Nueva Noticia");
            entity.setIdNoticia(0);
            entity.setFechaCreacion(new Date());
            entity.setMedioEntity(medioRepository.findOne(entity.getIdMedio()));
            noticiaFromDb = saveOrUpdate(entity, true);
        }

        noticiaFromDb.setRelevanciaUsuario(entity.getRelevanciaCalculada());
        noticiaFromDb.setSentimientoUsuario(entity.getSentimiento());
        noticiaFromDb.setEstatusUsuario(entity.getEstatusUsuario());
        noticiaFromDb.setSincronizado(true);

        if (entity.getTemas() != null) {
            cargarTemas(noticiaFromDb, entity.getTemas());
        }

        return saveOrUpdate(noticiaFromDb, true);
    }

    private void cargarTemas(Noticia noticiaFromDb, Set<NoticiaTema> noticiaTemaRequest) {
        for (NoticiaTema noticiaTemaFromDb : noticiaFromDb.getTemas()) {
            for (NoticiaTema noticiaTema : noticiaTemaRequest) {
                if (noticiaTemaFromDb.getNoticiaTemaPk() != null
                        && noticiaTemaFromDb.getNoticiaTemaPk().getTema() != null
                        && noticiaTemaFromDb.getNoticiaTemaPk().getNoticia() != null
                        && noticiaTemaFromDb.getNoticiaTemaPk().getTema().getIdTema() == noticiaTema.getNoticiaTemaPk()
                        .getTema().getIdTema()) {
                    actualizaNoticiaTema(noticiaTemaFromDb, noticiaTema, true);
                } else {
                    int idTema = noticiaTema.getNoticiaTemaPk().getTema().getIdTema();
                    Tema temaFromDb = temaRepository.findOne(idTema);
                    if (temaFromDb == null) {
                        throw new RuntimeException("No existe tema con id" + idTema);
                    }
                    noticiaTema.setNoticiaTemaPk(new NoticiaTemaPk(noticiaFromDb, temaFromDb));
                    agregaNoticiaTema(noticiaTema, true);
                }
            }
        }

    }

    private String getUser() {

        return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    @Override
    public String getUltimaActualizacion() {
        Date ultimaActualizacion = noticiaRepository.getMaxFechaCreacion();
        return DATE_FORMAT.format(ultimaActualizacion);
    }
}
