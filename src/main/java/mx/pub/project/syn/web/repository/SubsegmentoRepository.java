package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Subsegmento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SubsegmentoRepository extends CrudRepository<Subsegmento, Integer> {

    List<Subsegmento> findByTipoIdTipo(int idTipo);

    Subsegmento findByClave(String clave);
}
