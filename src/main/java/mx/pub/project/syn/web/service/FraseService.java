package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.Phrase;

import java.util.List;



public interface PhraseService {

    void activarPhrase(int idPhrase);

    void desactivarPhrase(int idPhrase);

    Phrase crearPhrase(Phrase Phrase);

    Phrase actualizarPhrase(Phrase Phrase);

    List<Phrase> listPhrases();

    Phrase getById(int id);

    List<Phrase> listPhrases(int idTema);

}
