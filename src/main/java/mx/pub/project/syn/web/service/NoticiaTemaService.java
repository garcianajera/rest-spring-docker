package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.NoticiaTema;

import java.util.List;



public interface NoticiaTemaService {

    NoticiaTema crear(NoticiaTema entity);

    NoticiaTema actualizar(NoticiaTema entity);

    List<NoticiaTema> list();

    NoticiaTema getById(int idNoticia, int idTema);
}
