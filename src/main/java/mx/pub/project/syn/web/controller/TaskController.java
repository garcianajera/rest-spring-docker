package mx.pub.project.syn.web.controller;

import mx.pub.project.syn.web.service.DataTransferService;
import mx.pub.project.syn.web.service.SincronizaInstitucionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    @Autowired
    private DataTransferService dataTransferService;

    @Autowired
    private SincronizaInstitucionesService sincronizaInstitucionesService;

    @GetMapping("/config")
    public void config() {
        dataTransferService.syncData();
    }

    @GetMapping("/noticias")
    public void noticias() {
        dataTransferService.loadNoticias();
    }

    @GetMapping("/instituciones")
    public void instituciones() {
        sincronizaInstitucionesService.sincronizaInstituciones();
    }

}
