package mx.pub.project.syn.web.controller;

import mx.pub.project.syn.web.entity.Subsegmento;
import mx.pub.project.syn.web.service.SubsegmentoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/subsegmento")
@Validated
public class SubsegmentoController {

    private final static Logger _logger = LoggerFactory.getLogger(SubsegmentoController.class);

    @Autowired
    private SubsegmentoService subsegmentoService;

    @GetMapping("/listr")
    public List<Subsegmento> getlist(@RequestParam(name = "idTipo", required = false) Integer idTipo) {
        if (idTipo != null) {
            _logger.debug("Get list of Subsegmento por tipo: " + idTipo);
            return subsegmentoService.list(idTipo);
        }
        _logger.debug("Get list of Subsegmento");
        return subsegmentoService.list();
    }

    @PostMapping("/crear")
    public void crear(@RequestBody Subsegmento entity) {
        _logger.debug("Crea Subsegmento");
        subsegmentoService.crear(entity);
    }

    @PostMapping("/actualizar")
    public void actualizar(@RequestBody Subsegmento entity) {
        _logger.debug("Actualiza Subsegmento");
        subsegmentoService.actualizar(entity);
    }

}
