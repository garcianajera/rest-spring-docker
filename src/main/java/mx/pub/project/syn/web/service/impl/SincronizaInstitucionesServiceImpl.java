package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.repository.*;
import mx.pub.project.syn.web.service.SincronizaInstitucionesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;



@Service
public class SincronizaInstitucionesServiceImpl implements SincronizaInstitucionesService {
    private final static Logger _logger = LoggerFactory.getLogger(SincronizaInstitucionesServiceImpl.class);

    @Autowired
    private InstitucionTempRepository institucionTempRepository;

    @Autowired
    private InstitucionRepository institucionRepository;

    @Autowired
    private TipoRepository tipoRepository;

    @Autowired
    private SubsegmentoRepository subsegmentoRepository;

    @Autowired
    private TemaRepository temaRepository;

    @Autowired
    private PhraseRepository PhraseRepository;

    @Override
    public void sincronizaInstituciones() {
        try {
            Date fechaUltimaActualizacion = institucionRepository.getMaxFechaActualizacion();
            _logger.info("Sincronizando instituciones, Fecha Ultima Actualizacion: " + fechaUltimaActualizacion);

            _logger.info("Leyendo instituciones of tabla temporal, con fecha actualizacion mayor a: "
                    + fechaUltimaActualizacion);
            List<InstitucionTemp> institucionTempAll = institucionTempRepository
                    .findAllByFechaActualizacionAfter(fechaUltimaActualizacion);

            _logger.info("Sincroniza tipos nuevos");
            sincronizaTipos(institucionTempAll);

            _logger.info("Sincroniza tipos subTipos");
            sincronizaSubsegmentos(institucionTempAll);

            _logger.info("Sincroniza instituciones");

            sincronizaInstituciones(institucionTempAll);
        } catch (Exception e) {
            _logger.error("Error sincronizando instituciones", e);
        }
    }

    private void sincronizaInstituciones(List<InstitucionTemp> institucionTempAll) {
        institucionTempAll.forEach(institucionTemp -> {
            List<Institucion> institucionFromDbList = institucionRepository
                    .findByClave(institucionTemp.getCveInstitucion());
            if (institucionFromDbList == null || institucionFromDbList.isEmpty()) {
                _logger.info("Nueva institucion con clave: " + institucionTemp.getCveInstitucion());
                // crea tema
                Tema tema = crearTema(institucionTemp);
                crearPhrase(institucionTemp, tema);
                Subsegmento subsegmento = subsegmentoRepository.findByClave(institucionTemp.getCveSubtipo());
                Institucion nuevaInstitucion = new Institucion();
                nuevaInstitucion.setIdTema(tema.getIdTema());
                nuevaInstitucion.setActivo(true);
                nuevaInstitucion.setClave(institucionTemp.getCveInstitucion());
                nuevaInstitucion.setFechaActualizacion(institucionTemp.getFechaActualizacion());
                nuevaInstitucion.setSubsegmentoEntity(subsegmento);

                institucionRepository.save(nuevaInstitucion);
            } else {
                String nombreCorto = institucionTemp.getNombreInst().length() > 80
                        ? institucionTemp.getNombreInst().substring(0, 80)
                        : institucionTemp.getNombreInst();
                Institucion institucionFromDb = institucionFromDbList.get(0);
                if (!institucionFromDb.getTema().getNombre().trim().equalsIgnoreCase(nombreCorto)) {
                    _logger.info("Actualiza institucion con clave: " + institucionTemp.getCveInstitucion());
                    Tema temaFromDb = institucionFromDb.getTema();
                    temaFromDb.setNombre(nombreCorto);
                    temaFromDb.setFechaActualizacion(institucionTemp.getFechaActualizacion());
                    temaFromDb.setSincronizado(false);
                    temaFromDb = temaRepository.save(temaFromDb);
                    crearPhrase(institucionTemp, temaFromDb);
                    institucionFromDb.setFechaActualizacion(institucionTemp.getFechaActualizacion());
                    institucionRepository.save(institucionFromDb);
                }
            }
        });

    }

    private Tema crearTema(InstitucionTemp institucionTemp) {
        String nombreCorto = institucionTemp.getNombreInst().length() > 80
                ? institucionTemp.getNombreInst().substring(0, 80)
                : institucionTemp.getNombreInst();
        Tema tema = new Tema();
        tema.setActivo(true);
        tema.setFechaActualizacion(institucionTemp.getFechaActualizacion());
        tema.setFechaCreacion(institucionTemp.getFechaActualizacion());
        tema.setNombre(nombreCorto);
        tema.setSincronizado(false);
        tema = temaRepository.save(tema);
        return tema;
    }

    private void crearPhrase(InstitucionTemp institucionTemp, Tema tema) {
        Phrase Phrase = new Phrase();
        Phrase.setActivo(true);
        Phrase.setFechaActualizacion(institucionTemp.getFechaActualizacion());
        Phrase.setFechaCreacion(institucionTemp.getFechaActualizacion());
        Phrase.setPhrase(institucionTemp.getNombreInst());
        Phrase.setIdTema(tema.getIdTema());
        Phrase.setPeso(1);
        Phrase.setSincronizado(false);

        PhraseRepository.save(Phrase);
    }

    private void sincronizaTipos(List<InstitucionTemp> institucionTempAll) {
        institucionTempAll.forEach(institucionTemp -> {
            Tipo tipo = tipoRepository.findByClave(institucionTemp.getCveTipo());
            if (tipo == null) {
                _logger.info("Nuevo tipo con clave: " + institucionTemp.getCveTipo());
                Tipo nuevoTipo = new Tipo();
                nuevoTipo.setClave(institucionTemp.getCveTipo());
                nuevoTipo.setDescripcion(institucionTemp.getDescTipo());
                nuevoTipo.setFechaActualizacion(institucionTemp.getFechaActualizacion());
                tipoRepository.save(nuevoTipo);
            }
        });
    }

    private void sincronizaSubsegmentos(List<InstitucionTemp> institucionTempAll) {
        institucionTempAll.forEach(institucionTemp -> {
            Subsegmento subsegmento = subsegmentoRepository.findByClave(institucionTemp.getCveSubtipo());
            if (subsegmento == null) {
                _logger.info("Nuevo subtipo con clave: " + institucionTemp.getCveSubtipo());
                Tipo tipo = tipoRepository.findByClave(institucionTemp.getCveTipo());
                Subsegmento nuevoSubsegmento = new Subsegmento();
                nuevoSubsegmento.setClave(institucionTemp.getCveSubtipo());
                nuevoSubsegmento.setDescripcion(institucionTemp.getDescSubtipo());
                nuevoSubsegmento.setFechaActualizacion(institucionTemp.getFechaActualizacion());
                nuevoSubsegmento.setTipo(tipo);
                subsegmentoRepository.save(nuevoSubsegmento);
            }
        });
    }

}
