package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Institucion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;


@Repository
public interface InstitucionRepository extends CrudRepository<Institucion, Integer> {

    List<Institucion> findBySubsegmentoEntityIdSubsegmento(int idSubsegmento);

    Set<Institucion> findBySubsegmentoEntityIdSubsegmentoIn(List<Integer> idSubsegmentos);

    @Query(value = "SELECT max(fecha_actualizacion) from im3_institucion", nativeQuery = true)
    Date getMaxFechaActualizacion();

    List<Institucion> findByClave(String clave);
}
