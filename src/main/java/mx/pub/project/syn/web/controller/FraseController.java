package mx.pub.project.syn.web.controller;

import mx.pub.project.syn.web.entity.Phrase;
import mx.pub.project.syn.web.service.PhraseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/phrase")
@Validated
public class PhraseConroller {
    private final static Logger _logger = LoggerFactory.getLogger(PhraseConroller.class);

    @Autowired
    private PhraseService PhraseService;

    @GetMapping("/list")
    public List<Phrase> getPhrases(@RequestParam(name = "idTema", required = false) Integer idTema) {
        if (idTema != null) {
            _logger.debug("Get list of Phrases por tema: " + idTema);
            return PhraseService.listPhrases(idTema);
        }
        _logger.debug("Get list of Phrases");
        return PhraseService.listPhrases();
    }

    @PutMapping("/{id}/activar")
    public void activaPhrase(@PathVariable(name = "id") int idPhrase) {
        _logger.debug("Activa Phrase: " + idPhrase);
        PhraseService.activarPhrase(idPhrase);
    }

    @PutMapping("/{id}/desactivar")
    public void desactivaPhrase(@PathVariable(name = "id") int idPhrase) {
        _logger.debug("Desactiva Phrase: " + idPhrase);
        PhraseService.desactivarPhrase(idPhrase);
    }

    @PostMapping("/crear")
    public void creaPhrase(@RequestBody Phrase Phrase) {
        _logger.debug("Crea Phrase");
        PhraseService.crearPhrase(Phrase);
    }

    @PostMapping("/actualizar")
    public void actualizar(@RequestBody Phrase Phrase) {
        _logger.debug("Actualiza Phrase");
        PhraseService.actualizarPhrase(Phrase);
    }

    @GetMapping("/{id}")
    public List<Phrase> getEntity(@PathVariable(name = "id") int id) {
        _logger.debug("Get Phrase:" + id);
        return PhraseService.listPhrases();
    }

}
