package mx.pub.project.syn.web.controller.

import mx.pub.project.syn.web.entity.Tema;
import mx.pub.project.syn.web.service.TemaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

pub.project.syn.web.controller;

@RestController
@RequestMapping("/api/tema")
@Validated
public class TemaController {

    private final static Logger _logger = LoggerFactory.getLogger(TemaController.class);

    @Autowired
    private TemaService temaService;

    @GetMapping("/listr")
    public List<Tema> getlist() {
        _logger.debug("Get list of Tema");
        return temaService.list();
    }

    @PutMapping("/{id}/activar")
    public void activar(@PathVariable(name = "id") int id) {
        _logger.debug("Activa Tema: " + id);
        temaService.activar(id);
    }

    @PutMapping("/{id}/desactivar")
    public void desactivar(@PathVariable(name = "id") int id) {
        _logger.debug("Desactiva Tema: " + id);
        temaService.desactivar(id);
    }

    @PostMapping("/crear")
    public void crear(@RequestBody Tema entity) {
        _logger.debug("Crea Tema");
        temaService.crear(entity);
    }

    @PostMapping("/actualizar")
    public void actualizar(@RequestBody Tema entity) {
        _logger.debug("Actualiza Tema");
        temaService.actualizar(entity);
    }

}
