package mx.pub.project.syn.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class NoticiaTemaPk implements Serializable {

    private static final long serialVersionUID = 1186433434673073478L;

    @ManyToOne
    @JoinColumn(name = "idNoticia")
    @JsonIgnore

    private Noticia noticia;

    @ManyToOne
    @JoinColumn(name = "idTema")
    private Tema tema;

    public NoticiaTemaPk() {
    }

    public NoticiaTemaPk(Noticia noticia, Tema tema) {
        super();
        this.noticia = noticia;
        this.tema = tema;
    }

    public Noticia getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticia noticia) {
        this.noticia = noticia;
    }

    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }

}
