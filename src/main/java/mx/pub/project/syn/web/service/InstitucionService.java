package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.Institucion;

import java.util.List;



public interface InstitucionService {

    void activar(int id);

    void desactivar(int id);

    Institucion crear(Institucion entity);

    Institucion actualizar(Institucion entity);

    List<Institucion> list();

    Institucion getById(int id);

    List<Institucion> listPorSubsegmento(int idSubsegmento);

    List<Institucion> listPorTipo(int idTipo);

}
