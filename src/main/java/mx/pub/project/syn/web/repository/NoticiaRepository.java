package mx.pub.project.syn.web.repository;

import mx.pub.project.syn.web.entity.Noticia;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface NoticiaRepository extends CrudRepository<Noticia, Integer>, NoticiaCustomRepository {

    List<Noticia> findByIsSincronizadoIsFalse();

    Noticia findByUrl(String url);

    @Query(value = "SELECT max(fecha_creacion) from im3_noticia", nativeQuery = true)
    Date getMaxFechaCreacion();

}
