package mx.pub.project.syn.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "im3_institucion_temp")
public class InstitucionTemp implements Serializable {

    private static final long serialVersionUID = 7462554068566385709L;

    @Id
    @Column
    private int id;
    @Column
    private String cveTipo;
    @Column
    private String descTipo;
    @Column
    private String cveSubtipo;
    @Column
    private String descSubtipo;
    @Column
    private String nombreInst;
    @Column
    private String cveInstitucion;
    @Column
    private Date fechaActualizacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCveTipo() {
        return cveTipo;
    }

    public void setCveTipo(String cveTipo) {
        this.cveTipo = cveTipo;
    }

    public String getDescTipo() {
        return descTipo;
    }

    public void setDescTipo(String descTipo) {
        this.descTipo = descTipo;
    }

    public String getCveSubtipo() {
        return cveSubtipo;
    }

    public void setCveSubtipo(String cveSubtipo) {
        this.cveSubtipo = cveSubtipo;
    }

    public String getDescSubtipo() {
        return descSubtipo;
    }

    public void setDescSubtipo(String descSubtipo) {
        this.descSubtipo = descSubtipo;
    }

    public String getNombreInst() {
        return nombreInst;
    }

    public void setNombreInst(String nombreInst) {
        this.nombreInst = nombreInst;
    }

    public String getCveInstitucion() {
        return cveInstitucion;
    }

    public void setCveInstitucion(String cveInstitucion) {
        this.cveInstitucion = cveInstitucion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

}
