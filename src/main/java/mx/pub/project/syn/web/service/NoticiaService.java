package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.Noticia;
import mx.pub.project.syn.web.entity.NoticiaFiltro;
import mx.pub.project.syn.web.entity.NoticiaResponse;

import java.util.List;



public interface NoticiaService {

    Noticia crear(Noticia entity);

    Noticia cargar(Noticia noticia);

    Noticia actualizar(Noticia entity);

    List<Noticia> list();

    Noticia getById(int id);

    NoticiaResponse list(NoticiaFiltro filtro);

    String getUltimaActualizacion();

}
