package mx.pub.project.syn.web.service;

import mx.pub.project.syn.web.entity.Tema;

import java.util.List;



public interface TemaService {

    void activar(int id);

    void desactivar(int id);

    Tema crear(Tema entity);

    Tema actualizar(Tema entity);

    List<Tema> list();

    Tema getById(int id);

}
