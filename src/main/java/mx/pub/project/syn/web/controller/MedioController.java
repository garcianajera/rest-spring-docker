package mx.pub.project.syn.web.controller;

import mx.pub.project.syn.web.entity.Medio;
import mx.pub.project.syn.web.service.MedioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/medio")
@Validated
public class MedioController {

    private final static Logger _logger = LoggerFactory.getLogger(MedioController.class);

    @Autowired
    private MedioService medioService;

    @GetMapping("/listr")
    public List<Medio> getlist() {
        _logger.debug("Get list of Medio");
        return medioService.list();
    }

    @PostMapping("/crear")
    public void crear(@RequestBody Medio entity) {
        _logger.debug("Crea Medio");
        medioService.crear(entity);
    }

    @PostMapping("/actualizar")
    public void actualizar(@RequestBody Medio entity) {
        _logger.debug("Actualiza Medio");
        medioService.actualizar(entity);
    }

    @PutMapping("/{id}/activar")
    public void activar(@PathVariable(name = "id") int id) {
        _logger.debug("Activa Medio: " + id);
        medioService.activar(id);
    }

    @PutMapping("/{id}/desactivar")
    public void desactivar(@PathVariable(name = "id") int id) {
        _logger.debug("Desactiva Medio: " + id);
        medioService.desactivar(id);
    }


}
