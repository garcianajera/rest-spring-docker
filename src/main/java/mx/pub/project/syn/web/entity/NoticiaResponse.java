package mx.pub.project.syn.web.entity;

import java.util.List;

public class NoticiaResponse {

    private List<Noticia> noticias;

    private Integer pagina;

    private Integer totalNoticias;

    private Integer totalPaginas;

    public List<Noticia> getNoticias() {
        return noticias;
    }

    public void setNoticias(List<Noticia> noticias) {
        this.noticias = noticias;
    }

    public Integer getPagina() {
        return pagina;
    }

    public void setPagina(Integer pagina) {
        this.pagina = pagina;
    }

    public Integer getTotalNoticias() {
        return totalNoticias;
    }

    public void setTotalNoticias(Integer totalNoticias) {
        this.totalNoticias = totalNoticias;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }


}
