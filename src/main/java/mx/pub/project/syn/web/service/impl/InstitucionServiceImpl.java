package mx.pub.project.syn.web.service.impl;

import mx.pub.project.syn.web.entity.Institucion;
import mx.pub.project.syn.web.repository.InstitucionRepository;
import mx.pub.project.syn.web.repository.SubsegmentoRepository;
import mx.pub.project.syn.web.service.InstitucionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



@Service
public class InstitucionServiceImpl implements InstitucionService {
    private final static Logger _logger = LoggerFactory.getLogger(InstitucionServiceImpl.class);

    @Autowired
    private InstitucionRepository institucionRepository;

    @Autowired
    private SubsegmentoRepository subsegmentoRepository;

    @Override
    public void activar(int id) {
        Institucion institucion = institucionRepository.findOne(id);
        if (institucion == null) {
            throw new RuntimeException("Institucion no existe");
        }
        institucion.setActivo(true);
        saveOrUpdate(institucion);

        _logger.info("Activando institucion " + institucion.getClave());
        _logger.info("Usuario: " + getUser());
    }

    @Override
    public void desactivar(int id) {
        Institucion institucion = institucionRepository.findOne(id);
        if (institucion == null) {
            throw new RuntimeException("Institucion no existe");
        }
        institucion.setActivo(false);
        saveOrUpdate(institucion);
        _logger.info("Desactivando institucion " + institucion.getClave());
        _logger.info("Usuario: " + getUser());
    }

    @Override
    public Institucion crear(Institucion entity) {
        _logger.info("Creando institucion " + entity.getClave());
        _logger.info("Usuario: " + getUser());
        return saveOrUpdate(entity);
    }

    @Override
    public Institucion actualizar(Institucion entity) {
        Institucion institucionFromDb = institucionRepository.findOne(entity.getIdTema());
        if (institucionFromDb == null) {
            throw new RuntimeException("Institucion no existe :" + entity.getIdTema());
        }

        if (entity.getClave() != null) {
            institucionFromDb.setClave(entity.getClave());
        }

        _logger.info("Actualizando institucion " + entity.getClave());
        _logger.info("Usuario: " + getUser());
        return saveOrUpdate(institucionFromDb);
    }

    @Override
    public List<Institucion> list() {
        List<Institucion> list = new ArrayList<>();
        Iterable<Institucion> iter = institucionRepository.findAll();
        iter.forEach(list::add);
        return list;
    }

    @Override
    public Institucion getById(int id) {
        return institucionRepository.findOne(id);
    }

    @Override
    public List<Institucion> listPorSubsegmento(int idSubsegmento) {
        return institucionRepository.findBySubsegmentoEntityIdSubsegmento(idSubsegmento);
    }

    @Override
    public List<Institucion> listPorTipo(int idTipo) {
        List<Integer> idSubsegmentos = new ArrayList<>();
        subsegmentoRepository.findByTipoIdTipo(idTipo)
                .forEach(subSegmento -> idSubsegmentos.add(subSegmento.getIdSubsegmento()));
        return new ArrayList<>(institucionRepository.findBySubsegmentoEntityIdSubsegmentoIn(idSubsegmentos));
    }

    private Institucion saveOrUpdate(Institucion entity) {
        entity.setFechaActualizacion(new Date());
        return institucionRepository.save(entity);
    }


    private String getUser() {
        return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
