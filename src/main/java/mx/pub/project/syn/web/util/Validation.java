package mx.pub.project.syn.web.util;

public class Validation {

    public static final String yearPattern = "^[0-9]{4}$";

    public static final String yearMessage = "El PathVariable year no es valido";

    public static final String monthPattern = "^([0][1-9]|[1][0-2])$";

    public static final String monthMessage = "El PathVariable month no es valido";

    public static final String idPattern = "^[a-zA-Z0-9]{1,25}$";

    public static final String idMessage = "El RequestParam id no es valido";

    public static final String institutionMessage = "El RequestParam institution no es valido";

    public static final String institutionTypePattern = "^[FGIPHSCV]{1}$";

    public static final String institutionTypeMessage = "El RequestParam institutionType no es valido";


    public static boolean validateYear(String year) {

        if (year == null)
            return false;

        return year.matches(yearPattern);
    }

    public static boolean validateMonth(String month) {

        if (month == null)
            return false;

        return month.matches(monthPattern);
    }

    public static boolean validateId(String id) {

        if (id == null)
            return false;

        return id.matches(idPattern);
    }

    public static boolean validateInstitutionType(String institutionType) {

        if (institutionType == null)
            return false;

        return institutionType.matches(institutionTypePattern);
    }
}
